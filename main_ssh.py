import sys
import subprocess
import time
import select
import streamlit as st
from pygtail import Pygtail
from ssh_data_addition import *
from ssh_parse import *
from ssh_predict import *
from colorama import init, Fore, Style
import threading

# Khởi tạo colorama
init(autoreset=True)

# Đường dẫn đến file log
auth_path = "/var/log/auth.log"

software_name = "SSH Detection"

# Khởi tạo các đối tượng cần thiết
up = Parse_SSH()
f = subprocess.Popen(['journalctl', '-u', 'ssh', '-f'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
p = select.poll()
p.register(f.stdout)
da = SSHDataAddition()
temp_dict = {}
ssh_p = SSHPerdiction()
link_git = "https://gitlab.com/nguyentri908908/ssh-detection-attack"
time_be_attack = 0
is_port_blocked = False

# Giao diện Streamlit
st.title("Phát hiện SSH Attack bằng Machine Learning")
st.write('Ứng dụng dựa trên ML này được phát triển cho mục đích giáo dục. Mục tiêu của ứng dụng là phát hiện SSH Attack')
st.write("Bạn có thể xem chi tiết về phương pháp, tập dữ liệu và tập đặc trưng nếu bạn nhấp vào gitlab sau: [link](%s)" % link_git)

st.write("Ứng dụng sẽ giám sát theo thời gian thực với server kali này...")

# Thêm khu vực để hiển thị kết quả trong Streamlit
alert_text = st.empty()
ssh_login_info = st.empty()
log_info_area = st.empty()

# Tạo bảng để lưu trữ các thông tin đăng nhập SSH
ssh_log_data = []

def update_ssh_login_info(info_dict):
    ssh_log_data.append(info_dict)
    login_info = ""
    for info in ssh_log_data:
        login_info = info
    ssh_login_info.markdown(login_info)

log_buffer = ""
log_counter = 0  # Biến đếm để tạo khóa duy nhất

def update_log_info(log):
    global log_buffer, log_counter
    log_counter += 1
    log_info_area.text_area(f"Log {log_counter}", value=log_buffer, height=400)
    log_buffer += log + "\n"

def block_ssh_port():
    global is_port_blocked
    try:
        # Thực hiện lệnh chặn cổng 22 sử dụng iptables
        subprocess.run(['sudo', 'iptables', '-A', 'INPUT', '-p', 'tcp', '--dport', '22', '-j', 'DROP'], check=True, encoding='ascii')
        st.success("Đã chặn cổng 22 thành công! ")
        is_port_blocked = True
        # Bắt đầu luồng để mở lại cổng sau 30 giây
        threading.Thread(target=unblock_ssh_port_after_delay).start()
    except subprocess.CalledProcessError as e:
        st.error(f"Lỗi khi thực hiện chặn cổng 22: {e}")

def unblock_ssh_port_after_delay():
    global is_port_blocked
    time.sleep(30)  # Đợi 30 giây
    try:
        # Thực hiện lệnh mở lại cổng 22 sử dụng iptables
        subprocess.run(['sudo', 'iptables', '-D', 'INPUT', '-p', 'tcp', '--dport', '22', '-j', 'DROP'], check=True, encoding='ascii')
        st.success("Đã mở lại cổng 22 sau 30 giây! Sẽ mở lại cổng sau 30s.")
        is_port_blocked = False
    except subprocess.CalledProcessError as e:
        st.error(f"Lỗi khi thực hiện mở lại cổng 22: {e}")

while True:
    if p.poll(1):
        temp = f.stdout.readline()
        temp = str(temp.decode("utf-8"))
        update_log_info(temp)  # Hiển thị log trực tiếp trong giao diện
        if "ssh" in temp:
            dict = up.SSHProcessed(temp)
            if not dict:
                continue
            if dict != temp_dict:
                update_log_info("=========================================")
                perdiction = ssh_p.predictSSH(ssh_p.prepareDict(dict))
                if perdiction > 0.90:
                    alert_text.markdown(f"<h2 style='color:red;'>Phần trăm bị tấn công: {perdiction:.2%} - Báo động đỏ!</h2>", unsafe_allow_html=True)
                    st.warning("Người dùng chú ý, server SSH đang bị tấn công!")
                    st.balloons()
                    time_be_attack += 1
                    if time_be_attack >= 8 and not is_port_blocked:
                        block_ssh_port()  # Chặn cổng 22 nếu phát hiện bị tấn công và chưa chặn trước đó
                else:
                    alert_text.markdown(f"<h2>Phần trăm bị tấn công: {perdiction:.2%}</h2>", unsafe_allow_html=True)
                    time_be_attack = 0  # Reset biến đếm
                    if is_port_blocked:
                        threading.Thread(target=unblock_ssh_port_after_delay).start()  # Nếu không bị tấn công, bắt đầu đếm lại để mở cổng
                update_ssh_login_info(dict)
                temp_dict = dict
                is_attack = 1 if perdiction > 0.95 else 0
                dict.update({"class": is_attack})
                da.writeSSH(dict)
            else:
                continue
    time.sleep(0.01)
